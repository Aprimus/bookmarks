import tempfile

from django.test import TestCase, override_settings
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.core.files.base import ContentFile

from images.views import ImageCreateView, ImageDetail, ImageLike
from account.utils import get_temporary_image
from images.models import Image


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class ImagesUrlsTestCase(TestCase):
    def setUp(self):
        self.photo = get_temporary_image()
        self.user_1 = User.objects.create_user(username='django', email='dj@em.com', password='1234')
        self.image = Image.objects.create(user=self.user_1,
                                          image=ContentFile(self.photo.read()),
                                          title='a test title')

    def test_create_use_correct_view(self):
        create_url = resolve(reverse('images:create'))
        self.assertEqual(create_url.func.view_class, ImageCreateView)

    def test_detail_view_use_correct_view(self):
        detail_view = resolve(reverse('images:detail', kwargs={'id': self.image.id,
                                                               'slug': self.image.slug}))
        self.assertEqual(detail_view.func.view_class, ImageDetail)

    def test_image_like_user_correct_view(self):
        like_url = resolve(reverse('images:like'))
        self.assertEqual(like_url.func.view_class, ImageLike)