import tempfile

from django.test import TestCase, override_settings
from django.contrib.auth.models import User

from account.utils import get_temporary_image
from images.models import Image


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class ImageModelTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username='django', email='django@ex.com', password='1234')
        self.user_2 = User.objects.create_user(username='alfa', email='alfa@ex.com', password='1234')
        self.user_3 = User.objects.create_user(username='beta', email='beta@ex.com', password='1234')
        self.photo = get_temporary_image()

    def tearDown(self):
        self.photo.close()

    def test_image_model_basic(self):
        self.assertEqual(Image.objects.count(), 0)
        image_inst = Image()
        image_inst.user = self.user_1
        image_inst.title = 'A test Image'
        image_inst.url = 'https:www.example.com/images/1'
        image_inst.description = 'A short test for image description'
        image_inst.save()

        first_db_entry = Image.objects.first()
        self.assertEqual(first_db_entry, image_inst)
        self.assertFalse(first_db_entry.image)
        self.assertEqual(first_db_entry.users_like.count(), 0)

        image_inst.users_like.add(self.user_2, self.user_3)  # M2M field
        image_inst.image = self.photo.name  # ImageField
        image_inst.save()

        self.assertEqual(Image.objects.count(), 1)
        self.assertEqual(str(first_db_entry), 'A test Image')
        self.assertEqual(first_db_entry.user, self.user_1)

        first_db_entry.refresh_from_db()

        self.assertTrue(first_db_entry.image)
        self.assertEqual(first_db_entry.users_like.count(), 2)
        self.assertEqual(first_db_entry.slug, 'a-test-image')
        self.assertEqual(first_db_entry.url, 'https:www.example.com/images/1')

        self.assertEqual(first_db_entry.get_absolute_url(), '/images/detail/1/a-test-image/')