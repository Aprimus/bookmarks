from unittest.mock import patch, MagicMock
import tempfile

from django.test import TestCase, override_settings
from django.contrib.auth.models import User

from images.forms import ImageCreateForm
from images.models import Image
from account.utils import get_temporary_image


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class ImageCreateFormTestCase(TestCase):
    def setUp(self):
        self.temp_image = get_temporary_image()
        self.temp_image.name = 'test.jpg'
        self.user_1 = User.objects.create_user(username='django', email='dj@em.com', password='1234')

    @patch('images.forms.request.urlopen')
    def test_image_creation_form_valid(self, mock_urlopen):
        mock = MagicMock()
        mock.status_code = 400
        mock.read.return_value = self.temp_image.read()
        mock_urlopen.return_value = mock

        url = 'http://www.example.com/images/cats.jpg'

        self.assertEqual(Image.objects.count(), 0)

        form = ImageCreateForm(data={
                                     'title': 'A Test Image',
                                     'url': url,
                                     'description': 'a short description about image'})
        self.assertTrue(form.is_valid())
        new_item = form.save(commit=False)
        new_item.user = self.user_1
        form.save()

        self.assertEqual(Image.objects.count(), 1)

        new_created_object = Image.objects.first()

        # Test the object creation

        self.assertEqual(new_created_object.title, 'A Test Image')
        self.assertEqual(new_created_object.url, url)
        self.assertTrue(new_created_object.image)
        self.assertIn('a-test-image', new_created_object.image.name)
