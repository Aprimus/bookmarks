import tempfile
from unittest.mock import patch, MagicMock

from django.test import TestCase, override_settings
from django.shortcuts import reverse
from django.contrib.auth.models import User

from images.forms import ImageCreateForm
from images.models import Image
from account.tests.tests_models import get_temporary_image
from account.models import Profile


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class ImageCreateViewTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username='django', email='django@em.com', password='1234')
        self.image = get_temporary_image()
        self.image.url = 'https://www.example.com/images/image1.jpg'

    def test_image_create_view_redirect_to_login(self):
        response = self.client.get(reverse('images:create'))
        self.assertEqual(response.status_code, 302)  # Redirect to login page
        self.assertEqual(response.url, 'login/?next=/images/create/')

    def test_image_create_view_auth_user(self):
        self.client.login(username='django', password='1234')
        response = self.client.get(reverse('images:create'))
        self.assertEqual(response.status_code, 200)

    def test_image_create_view_get(self):
        self.client.login(username='django', password='1234')
        response = self.client.get(reverse('images:create'), data={'url': self.image.url, 'title': 'test'})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.image.url)
        self.assertIsInstance(response.context['form'], ImageCreateForm)
        self.assertTrue(response.context['form'].is_bound)
        self.assertTrue(response.context['form'].is_valid())

    @patch('images.forms.request.urlopen')
    def test_image_create_view_post(self, mock_urlopen):
        mock = MagicMock()
        mock.status_code = 200
        mock.read.return_value = self.image.read()
        mock_urlopen.return_value = mock

        self.assertEqual(Image.objects.count(), 0)

        self.client.login(username='django', password='1234')
        response = self.client.post(reverse('images:create'), data={'title': 'A test title',
                                                                    'url': self.image.url,
                                                                    'description': 'a test description'})
        self.assertEqual(Image.objects.count(), 1)
        obj = Image.objects.first()
        self.assertEqual(obj.title, 'A test title')
        self.assertEqual(obj.url, self.image.url)
        self.assertEqual(obj.description, 'a test description')


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class ImageDetailViewTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username='django', email='dj@ex.com', password='1234')
        self.photo = get_temporary_image()
        self.photo.url = 'https://www.example.com/images/image.jpg'
        self.obj = Image.objects.create(user=self.user_1,
                                        title='a test title',
                                        url=self.photo.url,
                                        description='a test description',
                                        image=self.photo.name)

        self.profile_1 = Profile.objects.create(user=self.user_1, photo=self.photo)

    def test_image_detail_retrieve_correct_obj(self):
        self.assertEqual(Image.objects.count(),1)
        self.client.login(username='django', password='1234')
        response = self.client.get(reverse('images:detail', kwargs={'id': self.obj.id,
                                                                    'slug': self.obj.slug}))
        self.assertEqual(response.status_code, 200)

        # test get_context_data detail view
        self.assertEqual(response.context['section'], 'images')
        self.assertTemplateUsed(response, 'images/image/detail.html')
        self.assertEqual(response.context['image'].title, 'a test title')
        self.assertTrue(response.context['user'].profile.photo.url)


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class ImageLkeTestCase(TestCase):
    def setUp(self):
        self.photo = get_temporary_image()
        self.photo.url = 'http://www.example.com/images/image_1.jpg'
        self.user_1 = User.objects.create_user(username='django', email='dj@em.com', password='1234')
        self.image = Image.objects.create(
            user=self.user_1,
            title='A test Image',
            url=self.photo.url,
            image=self.photo,
            description='description test'
        )

    def test_image_like_view_raise_error_get(self):
        response = self.client.get(reverse('images:like'))
        self.assertEqual(response.status_code, 405)

    def test_image_like_view_post_like_unlike(self):
        self.assertEqual(Image.objects.count(), 1)
        self.assertEqual(list(self.image.users_like.all()), [])

        self.client.login(username='django', password='1234')
        self.assertTrue(self.user_1.is_authenticated)

        response = self.client.post(reverse('images:like'), data={'id': 1,
                                                                  'action': 'like'})
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'status': 'ok'})

        self.assertEqual(list(self.image.users_like.all()), [self.user_1])

        self.image.refresh_from_db()

        self.assertEqual(self.image.users_like.count(), 1)

        response = self.client.post(reverse('images:like'), data={'id': 1,
                                                                  'action': 'dislike'})
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'status': 'ok'})
        self.assertEqual(self.image.users_like.count(), 0)

    def test_image_like_view_incorrect_parameters(self):
        self.client.login(username='django', password='1234')
        response = self.client.post(reverse('images:like'), data={'id': '9999'})
        self.assertJSONEqual(str(response.content, encoding='utf8'), {'status': 'ko'})
