from django.urls import path

from . import views

app_name = 'images'

urlpatterns = [
    path('create/', views.ImageCreateView.as_view(), name='create'),
    path('detail/<int:id>/<slug:slug>/', views.ImageDetail.as_view(), name='detail'),
    # Ajax request
    path('like/', views.ImageLike.as_view(), name='like'),
]