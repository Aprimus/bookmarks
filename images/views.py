from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.views.generic import View, DetailView
from django.http import JsonResponse

from .forms import ImageCreateForm
from .models import Image


@method_decorator(login_required, name='dispatch')
class ImageCreateView(View):
    template_name = 'images/image/create.html'

    def get(self, request):
        form = ImageCreateForm(data=request.GET)
        return render(request, self.template_name, {'section': 'images', 'form': form})

    def post(self, request):
        form = ImageCreateForm(data=request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            new_item = form.save(commit=False)
            new_item.user = request.user
            new_item.save()
            messages.success(request, 'Image added successfully')
            return redirect(new_item.get_absolute_url())
        return render(request, self.template_name, {'section': 'images', 'form': form})


class ImageDetail(DetailView):
    model = Image
    template_name = 'images/image/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section'] = 'images'
        return context


class ImageLike(View):
    @method_decorator(login_required, name='dispatch')
    def post(self, request):
        image_id = request.POST.get('id')
        action = request.POST.get('action')
        if image_id and action:
            try:
                image = Image.objects.get(id=image_id)
                if action == 'like':
                    image.users_like.add(request.user)
                else:
                    image.users_like.remove(request.user)
                return JsonResponse({'status': 'ok'})
            except Image.DoesNotExist:
                pass
        return JsonResponse({'status': 'ko'})
