import tempfile
from datetime import date

from django.test import TestCase, override_settings
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.messages import get_messages


from account.forms import UserRegistrationForm, ProfileEditForm, UserEditForm
from account.models import Profile
from account.utils import get_temporary_image


class DashBoardViewTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username='user_1',
                                               email='user_1@email.com',
                                               password='secret1')

    def tearDown(self):
        self.client.logout()

    def tests_dash_board_view_logged_in_user(self):
        self.client.login(username='user_1', password='secret1')
        response = self.client.get(reverse('account:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'account/dashboard.html')
        self.assertEqual(response.context['section'], 'dashboard')

    def tests_dash_board_view_anonymous_user_redirect(self):
        response = self.client.get(reverse('account:dashboard'))
        self.assertEqual(response.status_code, 302)


class RegisterViewTestCase(TestCase):
    def test_register_view_GET(self):
        response = self.client.get(reverse('account:register'))
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context['form'], UserRegistrationForm)
        self.assertEqual(User.objects.count(), 0)
        self.assertFalse(response.context['form'].is_bound)
        self.assertTemplateUsed(response, 'account/register.html')

    def test_register_view_POST(self):
        self.assertEqual(User.objects.count(), 0)
        self.assertEqual(Profile.objects.count(), 0)
        data = {'username': 'Wicky',
                'first_name': 'John Wick',
                'email': 'wick@example.com',
                'password': 'secret1',
                'password2': 'secret1'}
        response = self.client.post(reverse('account:register'), data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(Profile.objects.count(),1)
        self.assertIsInstance(response.context['new_user'], User)
        self.assertEqual(response.context['new_user'].username, 'Wicky')
        self.assertTemplateUsed(response, 'account/register_done.html')

    def test_register_view_POST_duplicate_user(self):
        User.objects.create(username='Wicky',
                            first_name='John Wick',
                            password='secret1')
        data = {'username': 'Wicky',
                'first_name': 'John Wick',
                'email': 'wick@example.com',
                'password': 'secret1',
                'password2': 'secret1'}

        self.client.post(reverse('account:register'), data=data)
        self.assertEqual(User.objects.count(), 1)



class EditViewTestCase(TestCase):
    def setUp(self):
        self.image = get_temporary_image()
        self.user_1 = User.objects.create_user(username='django', email='hello@exam.com',password='secret1')
        self.profile_1 = Profile.objects.create(user=self.user_1)

    def test_edit_view_get_redirect_login(self):
        response = self.client.get(reverse('account:edit'))
        self.assertEqual(response.status_code, 302)

    def test_edit_view_get(self):
        self.client.login(username='django', password='secret1')
        self.assertTrue(self.user_1.is_authenticated)
        response = self.client.get(reverse('account:edit'))
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context['user_form'], UserEditForm)
        self.assertIsInstance(response.context['profile_form'], ProfileEditForm)
        self.assertFalse(self.profile_1.photo)


@override_settings(MEDIA_ROOT=tempfile.gettempdir())
class EditViewTestCasePost(TestCase):
    def test_edit_view_post(self):
        image = get_temporary_image()
        user_1 = User.objects.create_user(username='django', email='django@email.com', password='1234')
        profile_1 = Profile.objects.create(user=user_1)
        self.client.login(username=user_1.username, password='1234')
        self.assertTrue(user_1.is_authenticated)
        self.assertIsNone(user_1.profile.date_of_birth)
        self.assertFalse(user_1.profile.photo)
        response = self.client.post(reverse('account:edit'), data={'date_of_birth': date(year=2000, month=2, day=4),
                                                        'photo': image})
        user_1.refresh_from_db()
        self.assertIsNotNone(user_1.profile.date_of_birth)
        self.assertIsNotNone(user_1.profile.photo.name)

    def test_edit_view_messages_success(self):
        user_1 = User.objects.create_user(username='django', email='django@email.com', password='1234')
        profile_1 = Profile.objects.create(user=user_1)
        self.client.login(username='django', password='1234')
        response = self.client.post(reverse('account:edit'), data={'date_of_birth': date(year=2000, month=4, day=6)})
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual('Profile updated successfully', str(messages[0]))

    def test_edit_view_message_error(self):
        user_1 = User.objects.create_user(username='django', email='django@email.com', password='1234')
        profile_1 = Profile.objects.create(user=user_1)
        self.client.login(username='django', password='1234')
        response = self.client.post(reverse('account:edit'), data={'date_of_birth': 'error_date'})
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual('Error updating your profile', str(messages[0]))
# TODO move user creation on setup method
