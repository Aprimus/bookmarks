from datetime import date
import tempfile


from django.test import TestCase, override_settings
from django.contrib.auth.models import User


from account.forms import UserRegistrationForm, UserEditForm, ProfileEditForm
from account.models import Profile
from account.utils import get_temporary_image


class UserRegistrationFormTestCase(TestCase):
    def test_user_registration_form_valid_data(self):
        registration_form = UserRegistrationForm(data={'username': 'user_1',
                                                       'first_name': 'John Wick',
                                                       'email': 'wick@example.com',
                                                       'password': 'secret1',
                                                       'password2': 'secret1'})
        self.assertTrue(registration_form.is_valid())
        self.assertIn('Password', registration_form.as_p())
        self.assertIn('Repeat password', registration_form.as_p())

    def test_user_registration_form_invalid_passwords(self):

        registration_form = UserRegistrationForm(data={'username': 'user_1',
                                                       'first_name': 'wick@example.com',
                                                       'email': 'wick@example.com',
                                                       'password': 'secret1',
                                                       'password2': 'secret2'})
        self.assertFalse(registration_form.is_valid())
        self.assertEqual(registration_form.errors['password2'], ["Passwords don't match."])

    def test_user_registration_form_invalid_inputs(self):
        registration_form = UserRegistrationForm(data={'username': '',
                                                       'first_name': '',
                                                       'email': '',
                                                       'password': 'secret1',
                                                       'password2': 'secret2'})
        self.assertFalse(registration_form.is_valid())


class UserEditFormTestCase(TestCase):

    def test_user_edit_form_valid(self):
        user_edit_form = UserEditForm(data={'first_name': 'John',
                                            'last_name': 'Doo',
                                            'email': 'doo@email.com'})
        self.assertTrue(user_edit_form.is_valid())
        self.assertEqual(user_edit_form.errors, {})


class ProfileEditFormTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username='django', email='email@email.com', password='1234')
        self.profile_1 = Profile.objects.create(user=self.user_1)
        self.image = get_temporary_image()

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_profile_edit_form_valid(self):
        self.assertFalse(self.user_1.profile.photo)
        files = {'photo': self.image}

        profile_edit_form = ProfileEditForm(instance=self.profile_1, data={'date_of_birth': date(month=2, year=1978, day=2)},
                                            files=files)
        profile_edit_form.save()
        self.assertIsNotNone(self.user_1.profile.date_of_birth)
        self.assertIsNotNone(self.user_1.profile.photo)