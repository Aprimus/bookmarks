from django.test import TestCase
from django.contrib.auth.models import User
from django.shortcuts import reverse


class EmailBackendAuthTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user(username='django',
                                            email='django@em.com',
                                            password='1234')
        cls.redirect_login = '/account/login/?next=/account/'

    def test_login_with_username(self):
        login = self.client.login(username='django', password='1234')
        response = self.client.get(reverse('account:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.context['user']), 'django')

    def test_login_with_email(self):
        self.client.login(username='django@em.com', password='1234')
        response = self.client.get(reverse('account:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.context['user']), 'django')

    def test_login_with_incorrect_email(self):
        self.client.login(username='wrong@email.com', password='1234')
        response = self.client.get(reverse('account:dashboard'))
        self.assertRedirects(response, status_code=302, expected_url=self.redirect_login)

    def test_login_with_incorrect_username(self):
        self.client.login(username='foo', password='1234')
        response = self.client.get(reverse('account:dashboard'))
        self.assertRedirects(response, status_code=302, expected_url=self.redirect_login)