from datetime import date

from django.test import TestCase
from django.contrib.auth.models import User


from account.models import Profile
from account.utils import get_temporary_image


class ProfileModelTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create(username='Django')
        self.image = get_temporary_image()

    def tearDown(self):
        self.image.close()

    def test_basic_profile_model(self):
        self.assertEqual(Profile.objects.count(), 0)

        profile_1 = Profile.objects.create(user=self.user_1,
                                           date_of_birth=date(year=1978, month=2, day=28),
                                           photo=self.image.name)
        self.assertEqual(Profile.objects.count(), 1)
        self.assertEqual(profile_1.photo, self.image.name)