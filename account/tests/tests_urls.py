from django.test import TestCase
from django.urls import reverse, resolve

from account.views import dashboard, Register


class AccountUrlsTestCase(TestCase):

    def test_dashboard_url_use_correct_view(self):
        dashboard_url = resolve(reverse('account:dashboard'))
        self.assertEqual(dashboard_url.func, dashboard)

    def test_register_use_correct_view(self):
        register_url = resolve(reverse('account:register'))
        self.assertEqual(register_url.func.view_class, Register)