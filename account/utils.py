import io
from django.core.files.uploadedfile import SimpleUploadedFile
from PIL import Image


def get_temporary_image():
    file = io.BytesIO()
    image = Image.new('RGB', (200,200), 'white')
    image.save(file, 'PNG')
    file.seek(0)
    return SimpleUploadedFile('test.png', file.read(), content_type='image')